# Heroku

[Heroku documentation](https://devcenter.heroku.com/categories/reference)

## What is Heroku?

Heroku is a managed cloud platform as a service provider. It enables developers to very quickly and easily spin up instances of their app with very little configuration. Heroku manages the SSL certificates and other intricacies of cloud hosting.

## How does it work?

Your app is first compiled by the Slug compiler into Slug. These can be thought of as images. Apps are then run from these Slugs in what are called 'dynos'.

Slugs can be created from Dockerfiles or plain source code with a [buildpack](#buildpacks) which automatically installs all the dependencies needed at runtime. The mechanism by which this is done depends on the language being used, so please consult the documentation.

## App Requirements

Requirements and dependencies for an application should be added to a file in the root directory. The name of this file depends on the language being used. A few examples can be found below, but please consult the documentation:

- **Python:** `requirements.txt`
- **PHP:** `composer.json`
- **Go:** `go.mod`
- **Node.js:** `packages.json`

## Procfile

[The Procfile](https://devcenter.heroku.com/articles/procfile)

- The Procfile contains information on how to execute your app on runtime
- Must be in the root directory and must be called `Procfile`

A Procfile declares its process types on individual lines in the following format:

`<process-type>: <command>`

The `process-type` is a name for your command. You can call a process whatever you want, however there are two special process types:

- The `web` process is the only type that can be accessed externally by HTTP traffic
- The `release` process which is used within [pipelines](#pipelines) to define a release script
  
Although for most apps just a `web` process is enough, there can be more than one process in a Procfile with one definition per line. This is useful for worker processes and allows for more that one process per git repository.

Heroku assigns a dynamic port to the app internally using the `PORT` environment variable so, depending on the contents of the app, it must have it's exposed port set to the environment variable `PORT`.

## Buildpacks

A buildpack is a set of scripts that build a project. The output is compiled using the slug compiler which is then run as a Heroku Dyno. There are a number of [officially supported buildpacks](https://devcenter.heroku.com/articles/buildpacks#officially-supported-buildpacks) for various languages.

The correct buildpack is selected through an automated process but it can be manually selected using the Heroku CLI or an [app.json](#appjson-file) file.

## Addons

There are many 3rd party addons that can be used with apps on Heroku. These include:

- Persistent logging with Papertrail or Logtail
- Persistent storage using object storage (S3), SQL, NoSQL and many more
- Email and SMS integration
- Caching using Redism Memcached, Fastly e.t.c

A full list can be found [here.](https://elements.heroku.com/addons)

## app.json file

[app.json Schema](https://devcenter.heroku.com/articles/app-json-schema)

The `app.json` file is used to define parameters for the application. There are a lot of parameters that can be set, so please refer to the documentation.

It is not typical to use an `app.json` file as any important parameters can be set using the CLI either manually or using a CI pipeline.

## Pipelines

[Pipelines Documentation](https://devcenter.heroku.com/articles/pipelines)

A pipeline is a group of apps for the same project. There are 4 stages within a pipeline:

- Development
- Review
- Staging
- Production

Apps can move between these stages and CI scripts can be added to different stages of the pipeline using Github.

Each pull request in Github will get it's own running app for very straight-forward reviews.

Apps in the development, review and staging stages are free to run, however they enter a sleep state after 1 hour of no activity. This requires the app to be woken once a request comes in which will incur a short delay which is not idea in a production environment. See [app states](#states) for more information.

## App States

There are four states that an app can be:

- Awake and will never sleep
- Awake but will sleep after 1 hour of no activity
- Asleep
- Not running

They each have their own symbols in the dashboard. See this [blog post](https://blog.heroku.com/app_sleeping_on_heroku) for more information.

## Heroku CLI

[Documentation page](https://devcenter.heroku.com/categories/command-line)

The CLI interface is essential for working with Heroku. It gives the ability to do anything and everything with Heroku and is commonly used within Gitlab CI scripts to automate deployment.

Common commands:

- **heroku create**: Initializes the repo with a remote called `heroku` and then generates a new app in the heroku platform
- **git push heroku main**: This pushes to the Heroku remote which will then build and deploy the app
- **heroku log --tail**: Shows logs for the heroku app
- **heroku open**: Opens the app in the browser
- **heroku apps**: Gives a list of all apps that the user has
- **heroku apps:info**: Gives a detailed description of the app that is in the pwd

The Heroku CLI will apply these commands by default to the app that is associated with the git repo that has been selected. A specific app can be selected with `--app <app_name>`.

## Deploying Static Files

[From this Github Gist](https://gist.github.com/wh1tney/2ad13aa5fbdd83f6a489)

Assuming `index.html` is on the root directory:

```bash
echo "{}" > composer.json
mv index.html home.html
echo "<?php include_once("home.html"); ?>" > index.php
```

The empty `composer.json` file is there to 'trick' Heroku into using the PHP buildpack.

## Deploying From Gitlab CI

Deploying to Heroku using a CI pipeline is very easy by using the `dpl` Ruby gem:

```yaml
script:
  - gem install dpl
  - dpl --provider=heroku --app=<app-name> --api-key=$HEROKU_API_KEY
```

## Deploying Docker Containers

Firstly, the repo must contain a `heroku.yml` file. This simple points to the Dockerfile you want to build for the app:

```yaml
build:
  docker:
    web: Dockerfile
```

Secondly, the Heroku app must be configured with the Docker runner. This can be done with `heroku stack:set container` from within the git repo directory.

Key note:

- Reduce the number of layers in the Dockerfile as much as possible as Heroku has a limit to the size of each build. Smaller images will build more reliably

## Local Storage

Apps on Heroku are ephemeral (like docker containers), meaning they do not persist on restarts. Therefor it is essential to use external storage mechanisms such as object storage (S3) or the database of your choice. For instructions on creating a persistent database in Heroku see [below.](#creating-a-heroku-database-instance)

## Creating A Heroku Database Instance

This can be used on a free tier, however billing will need to be enabled for this to work.

1. Enter the Heroku dashboard and select 'create new app' or select and existing app
2. Go to 'Resources' > 'Find more add-ons'
3. Find 'ClearDB MySQL' (or the DB of your choice) and click 'Install ClearDB MySQL'
4. Select the plan and provision to your app
5. Once the DB has been provisioned, go to setting and select 'Reveal config vars'
6. Grab the value from the `CLEARDB_DATABASE_URL` variable
7. The format will look something like this:

`mysql://<username>:<password>@<hostname>/heroku_<somenumber>?reconnect=true`

Use the connection string or extract the username, password and hostname for use in your application.
