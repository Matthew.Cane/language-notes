Directory Tree
==============

├──
[AWS](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/AWS/)  
│   ├──
[Aurora.drawio](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/AWS/Aurora.drawio)  
│   ├──
[Aurora.png](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/AWS/Aurora.png)  
│   ├──
[AWS\_CLI.md](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/AWS/AWS_CLI.md)  
│   ├──
[AWS.md](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/AWS/AWS.md)  
│   ├──
[Cloudfront.md](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/AWS/Cloudfront.md)  
│   ├──
[EKS.md](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/AWS/EKS.md)  
│   ├──
[Glue.md](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/AWS/Glue.md)  
│   ├──
[IAM.md](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/AWS/IAM.md)  
│   ├──
[RDS.md](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/AWS/RDS.md)  
│   ├──
[S3.md](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/AWS/S3.md)  
│   └──
[SQS.md](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/AWS/SQS.md)  
├──
[devops\_roadmap.png](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/devops_roadmap.png)  
├──
[DNS.md](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/DNS.md)  
├──
[GitLab](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/GitLab/)  
│   ├──
[Gitlab\_CICD.jpg](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/GitLab/Gitlab_CICD.jpg)  
│   └──
[Gitlab.md](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/GitLab/Gitlab.md)  
├──
[heroku.md](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/heroku.md)  
├──
[ISO27001.md](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/ISO27001.md)  
├──
[Kubernetes](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/Kubernetes/)  
│   ├──
[cert-manager.md](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/Kubernetes/cert-manager.md)  
│   ├──
[helm.md](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/Kubernetes/helm.md)  
│   ├──
[k8s\_config\_diagram.drawio](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/Kubernetes/k8s_config_diagram.drawio)  
│   ├──
[k8s\_config\_diagram.png](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/Kubernetes/k8s_config_diagram.png)  
│   ├──
[k8s\_manifests.md](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/Kubernetes/k8s_manifests.md)  
│   ├──
[kubectl.md](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/Kubernetes/kubectl.md)  
│   ├──
[Kubernetes.md](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/Kubernetes/Kubernetes.md)  
│   └──
[mongo\_example\_deployment](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/Kubernetes/mongo_example_deployment/)  
│       ├──
[express-deployment.yaml](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/Kubernetes/mongo_example_deployment/express-deployment.yaml)  
│       ├──
[mongo-configmap.yaml](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/Kubernetes/mongo_example_deployment/mongo-configmap.yaml)  
│       ├──
[mongo-deployment.yaml](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/Kubernetes/mongo_example_deployment/mongo-deployment.yaml)  
│       └──
[mongo-secret.yaml](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/Kubernetes/mongo_example_deployment/mongo-secret.yaml)  
├──
[nginx\_cheatsheet.md](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/nginx_cheatsheet.md)  
├──
[rails\_notes.md](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/rails_notes.md)  
├──
[ruby\_notes.md](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/ruby_notes.md)  
├──
[salesforce.md](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/salesforce.md)  
└──
[Terraform](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/Terraform/)  
    ├──
[Examples](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/Terraform/Examples/)  
    │   └──
[ec2-example.tf](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/Terraform/Examples/ec2-example.tf)  
    └──
[Terraform.md](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/Terraform/Terraform.md)  
