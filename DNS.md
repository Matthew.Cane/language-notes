# Domain Name System

Notes compiled by [Matthew Cane](https://gitlab.com/Matthew.Cane) from the following content:

- [Cloudflare Learning: DNS Records](https://www.cloudflare.com/en-gb/learning/dns/dns-records/)

## Common Domain Types

### A

A record that holds the IP address of a domain.

```zone
example.com Type: Value:  TTL:
@           A     1.2.3.4 14400
```

### AAA

The same as A, but uses IPv6 addresses rather than IPv4 addresses.

### CNAME

Give one domain or subdomain the IP record of another domain. Short for 'canonical name'.

```zone
example.com Type: Value:       TTL:
@           CNAME example2.com 14400
```

### ALIAS or ANAME

Non-standard, these come in many different names.

ANAME is like CNAME but points to the root of the domain

### MX

Mail server records. Points to a mail server.

```zone
example.com Type: Priority: Value:           TTL:
@           MX    10        mail.example.com 14400
```

MX records also have a priority. This allows for multiple MX records for the same domain to be set up. Traffic will be routed to the entry with the highest priority that is available.

### TXT

Gives admins the ability to store short strings in the domain records.

```zone
example.com Type: Value:      TTL:
@           TXT  "Some Text" 14400
```

While traditionally only used for commenting, it is possible to put machine readable values, for example for email spam protection.

### NS

NS (nameserver) records indicate which DNS server is the authority for that domain.

```zone
example.com Type: Value:         TTL:
@           NS    ns.example.com 14400
```

A domain often has multiple NS records for backup nameservers in order to improve reliability.

### SOA

Short for 'start of authority', this contains information about the domain or zone. All zones in a DNS record require an SOA.

```zone
name example.com 
record type: SOA
MNAME ns.example.com
RNAME admin.example.com
SERIAL 1
REFRESH 86400
RETRY 7200
EXPIRE 400000
TTL 11200
```

- **Serial** is a version number for the SOA record that secondary nameservers can see and be prompted to update their own record
- **Refresh** is the time in seconds that secondary servers should wait to see if the record has been updated
- **Retry** is the length of time a server should wait for an unresponsive nameserver
- **Expire** is the time to wait for an unresponsive nameserver to respond before stopping queries to that nameserver

### SRV

### PTR
