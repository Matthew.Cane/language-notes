# Prometheus

## What is Prometheus

- An open-source metrics based monitoring service written in Go.
- Pull metrics from an endpoint
- Excels with microservices
- Runs stand-alone and independent of the rest of the architecture

## What ISN'T Prometheus

- Not designed for long-term storage, 3 to 6 months on average
- Is not 100% accuracy, better for summaries
- Pulls metrics, not pushes (generally)

## Feature Set

- Time-series data identification
  - Uses key-value pairs
  - Gives a name to each metric
- Functional query language
  - Uses PromQL
  - Performs real-time aggregation
  - Is graph based
  - Can be consumed by external services such as an HTTP API
  - Pre-configured rules can speed up longer queries
- Web dashboard
  - Uses PromDash
  - Has SQL backend

## Components

- Prometheus server
  - Retrival workers
  - Time-series database
  - HTTP server
    - Web UI
- PushGateway
- AlertManager

## Prometheus Server Component

## PromQL

A graph query language.

### Rules

- PromQL uses a configuration file to define recording rules, found in the config at `rule_files: - prometheus-rules.yml`
- There can be multiple rule files
- Written in yaml

An example rule:

```yaml
groups:
  - name: cpu-node
    rules:
      - record: job_instance_mode:node_cpu_seconds:avg_rate5m
        expr: avg by (job, instance, mode) (rate(node_cpu_seconds_total[5m]))
```

## PushGateway

- Used to cache metrics from short-lived, service-level jobs so the Prometheus server can collect them later
- Best used for capturing service-level jobs, not system-level information
- PushGateway metrics collected from service-level jobs are persisted after the job as completed

## Exporters

Open source libraries that open a web server containing metrics that Prometheus can scrape from. There are many existing exporters