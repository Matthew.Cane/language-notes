# Amazon Database Service

Notes compiled by [Matthew Cane](https://gitlab.com/Matthew.Cane) from the following content:

- [Amazon Aurora Introduction](https://youtu.be/ZCt3ctVfGIk)
- [AWS Aurora Serverless Tutoral | Step By Step](https://youtu.be/ciRbXZqBl7M)
- [Getting started with Amazon RDS](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_GettingStarted.html)
- [What is Amazon Aurora?](https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/CHAP_AuroraOverview.html)

## What is RDS

- RDS is Amazon's cloud managed relational database service.
- RDS can host many types of relational databases:
  - MariaDB
  - MySQL
  - Microsoft SQL Server
  - Oracle
  - PostgreSQL
  - Aurora

## Aurora

- Proprietary Amazon technology
- Compatible with MySQL and Postgres
- 5x faster than MySQL, 3x faster than Postgres
- Automatically grows in increments of 10gb up to 64tb
- Can have up to 15 replicas
- Instant failover
- Costs 20% more than other RDS databases
- Stores 6 copies across 3 availability zones
- Stipes data across many (100s) of volumes, but all is abstracted away
- By default has 1 write master node but up to 15 auto-scaling read nodes
- A read node can become a master write node in the event of failover

![Aurora Cluster Layout](Aurora.png)

### Endpoints

- There are two endpoints that exist for each DB instance:
  - A writer endpoint that redirects requests to the current write master
  - A reader endpoint that load balances across the read nodes. Load balancing is done at the connection level, not the query level
