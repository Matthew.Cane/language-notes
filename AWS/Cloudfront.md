# AWS Cloudfront

Notes compiled by [Matthew Cane](https://gitlab.com/Matthew.Cane) from the following content:

- [Cloudfront Developer Guide](https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/)

## What is Cloudfront?

Cloudfront is a CDN provided by AWS.

If the content has been cached it will route requests to the edge location that provides the lowest latency to the user. Otherwise it will route to the content origin.

It also acts as a TLS endpoint and allows for SSL certificates from ACM to be applied to the service.

The cache is flushed either manually or using a TTL value.

## How static sites are hosted using S3 & Cloudfront

1. Static site files are added to an S3 bucket. Usually, the bucket name would be the domain name of the site for clarity
2. Within the S3 bucket, static web hosting must be enabled under properties
3. In Cloudfront, create a new distribution.
4. Enter the origin domain name as the URL for your S3 bucket.
5. Certificates can be acquired by choosing a custom SSL certificate and creating a new certificate in ACL.

There are many other configuration options that are available, depending on the project. These include:

- Changing the default root object and root path
- Changing the region for cost savings
- Geo-restricting requests
- Redirecting HTTP to HTTPS
- Allowing HTTP methods
- Configuring CORS headers
- Adding custom headers
- Triggering Lambda functions

Cache clearing, also known as invalidations, can be created to manually force certain files to refresh. This can be done in the console or through the AWS-CLI
