# Identity & Access Management

- [AWS re:Invent 2018: Become an IAM Policy Master in 60 Minutes or Less](https://youtu.be/YQsK4MtsELU)
- [AWS IAM Overview in 7 minutes | Beginner Overview](https://youtu.be/y8cbKJAo3B4)

## Key Components

### Users

A specific individual, human or non-human.

### Groups

A collection of users.

### Roles

A collection of policies.

### Policies

Low level permission to resources:

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "<statement id>",
            "Effect": "<Allow/Deny>",
            "Action": [
                "<aws service>:<operation>",
                "<aws service>:<operation>"
            ],
            "Resource": "<resource name>"
        }
    ]
}
```

Below is an example policy granting put, get, delete and ACL change on all folders within a particular bucket, in this case `matthewcane.co.uk`:

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:ListBucket"
            ],
            "Resource": [
                "arn:aws:s3:::matthewcane.co.uk"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:PutObject",
                "s3:GetObject",
                "s3:DeleteObject",
                "s3:PutObjectAcl"
            ],
            "Resource": [
                "arn:aws:s3:::matthewcane.co.uk/*"
            ]
        }
    ]
}
```

## Best Practices

- Use least privilege model
- Exercise caution when modifying existing policies
