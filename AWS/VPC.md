# Virtual Private Cloud (VPC)

Notes compiled by [Matthew Cane](https://gitlab.com/Matthew.Cane) from the following content:

- [AWS Certified Solutions Architect (Full Course)](https://youtu.be/Ia-UEYYR44s)
- [What is Amazon VPC?](https://docs.aws.amazon.com/vpc/latest/userguide/what-is-amazon-vpc.html)
- [Virtual private clouds (VPC)](https://docs.aws.amazon.com/vpc/latest/userguide/configure-your-vpc.html)

> A virtual private cloud (VPC) is a virtual network dedicated to your AWS account. It is logically isolated from other virtual networks in the AWS Cloud. You can launch your AWS resources, such as Amazon EC2 instances, into your VPC.

A VPC:

- Can only exist in a single region but spans across availability zones
- Supports IPv4 and optionally IPv6 addressing
- 

## Subnets

See: https://docs.aws.amazon.com/vpc/latest/userguide/configure-subnets.html

## IP Addressing

## Routing

See: https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Route_Tables.html

## Gateways and Endpoints

See:

- https://docs.aws.amazon.com/vpc/latest/userguide/extend-intro.html
- https://docs.aws.amazon.com/vpc/latest/privatelink/privatelink-access-aws-services.html

## Peering Connections

See: https://docs.aws.amazon.com/vpc/latest/peering/

## Traffic Monitoring

See: https://docs.aws.amazon.com/vpc/latest/mirroring/

## Transit Gateways

## Flow Logs

## VPN Connections
