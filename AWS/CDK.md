# AWS CDK

Notes compiled by [Matthew Cane](https://gitlab.com/Matthew.Cane) from the following content:

- [CDK v2 Development Guide](https://docs.aws.amazon.com/cdk/v2/guide/home.html)
- [AWS CDK Primer Lab](https://explore.skillbuilder.aws/learn/course/external/view/elearning/1475/aws-cloud-development-kit-primer)

## What is AWS CDK?

An infrastructure-as-code tool that build Cloudformation stacks using common programming languages:

- TypeScript
- JavaScript
- Python
- Java
- C#

It compiles the code into Cloudformation JSON objects and uploads it via the AWS CLI to the Cloudformation platform which then executes it.

## Common CDK Commands

- `ls`: Lists all stacks in the app
- `synth`: Generates and prints the generated Cloudformation template
- `bootstrap`: Prepares your targeted AWS environment (combination of AWS account and region) for deploying a stack to by provisioning certain resources such as IAM roles
- `deploy`: Deploys the stack(s) into your AWS account
- `import`: Imports existing resources into your stack
- `doctor`: Checks your AWS CDK project for potential problems

## Key Concepts

### Constructs

See: https://docs.aws.amazon.com/cdk/v2/guide/constructs.html

> Constructs are the basic building blocks of AWS CDK apps. A construct represents a "cloud component" and encapsulates everything AWS CloudFormation needs to create the component.

A "cloud component" can be a single resource, like an S3 bucket, or a collection of resources that make up a single feature, such as an SNS topic and it's subscriptions.

Examples of pre-build constructs can be found in the [AWS Construct Hub](https://constructs.dev/search?q=&cdk=aws-cdk&cdkver=2&sort=downloadsDesc&offset=0). These can be re-used in your own projects.

There are three layers of constructs:

- **Layer 1**: Represent raw API resources that require all resource properties to be explicitly configured. Also called *`Cfn` resources* or *Resource Constructs*.
- **Layer 2**: Also represent API resources, but with a layer of abstraction. They have pre-applied sensible default values and helpful functions for ease of use. Also called *AWS Constructs*.
- **Layer 3**: A collection of constructs that are tightly coupled which  provide specific functionality, for example an API gateway with a Lambda function behind it. These are also called *Patterns*

### Compositions

> Composition is the key pattern for defining higher-level abstractions through constructs. A high-level construct can be composed from any number of lower-level constructs, and in turn, those could be composed from even lower-level constructs, which eventually are composed from AWS resources.

It is, essentially, a library of infrastructure resources and their configuration that can be shared with others and used in their own stacks.

### Initialing Constructs

Constructs are implemented in classes that extend the `Construct` base class. All constructs take three parameters when initialised:

- **Scope**: The constructs parent or owner, either the top-level `stack` object or another construct. Usually this will be `self`
- **ID**: An identifier that is unique within the scope. Used to generate the logical ID's in CloudFormation.
- **Props**: A set of keywork arguments (`**kwargs`) that defines the constructs initial configuration. In most cases constructs the props are optional as sensible defaaults are already set.

### Stacks

Stacks are logical groups of constructs that can be structured in whatever way suites the project best. They provide a context in which to define your constructs. You may have a 'data' stack with databases and caches, a 'network' layer with VPCs, firewalls, API gateways, e.t.c, and an 'application' stack with Lambda functions or EC2 instances.

A stack extends the base `Stack` class. Stacks are deployed into an AWS Environment (combination of AWS account and region).

### Apps

A CDK application is called an **app** which is represented by a CDK class `App`. An app can contain one or more stacks.

### Changesets

See: https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/using-cfn-updating-stacks-changesets.html

This is a Cloudformation concept that closely mimics the functionalily of the the `cdk diff` command, but saves the output to the stack in the Cloudformation console

### Bootstrapping

See: https://docs.aws.amazon.com/cdk/v2/guide/bootstrapping.html

> Deploying AWS CDK applications into your AWS account might require that you provision resources the AWS CDK needs to perform the deployment.

This could include creating S3 buckets to store Lambda code, creating [IAM service roles](#service-roles) or adding tags to the Cloudformation resources

### Service Roles

Service roles are another Cloudformation concept. When `cdk bootstrap` is performed, CDK will create service roles that the IAM user executing the task will be able to assume (or pass to Cloudformation? Unclear).

They will be in the format of:

`cdk-<random-value>-deploy-role-<account number>-<region>`
ß
The account that executes the `cdk deploy` command must have the IAM permissions to assume these roles, e.g:

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AssumeCDKRole",
            "Effect": "Allow",
            "Action": "sts:AssumeRole",
            "Resource": "arn:aws:iam::017883735307:role/cdk-*"
        }
    ]
}
```

### Tests

See:

- https://docs.aws.amazon.com/cdk/v2/guide/testing.html
- https://docs.pytest.org/en/7.1.x/

Tests can be run with pytest using the command `python3 -m pytest` from the root of the project. Unit tests follow the pytest convention, where the test script files are located in `tests/unit/` and the filenames either begin with `test_` or end with `_test`.

#### Snapshot Tests

> Snapshot tests assert that the AWS CloudFormation template that your AWS CDK code generates is the same as it was at test creation. If any code changes, the test framework shows you the changes in a diff output.

A [plugin](https://github.com/tophat/syrupy) is required for Pytest in order to use this feature.

A snapshot of a stack can be made, which can then be used as a comparison to future stack builds.

[Python example of snapshot tests.](https://github.com/cdklabs/aws-cdk-testing-examples/tree/main/python)

### Identifers

These are used to identify constructs in your app. They are:

- Construct IDs
  - Must be unique within a scope
  - Used to identify individual constructs
- Paths
  - Represents the hierarchy within an app
  - For example, `MyStack/MyVpc/Resource`
  - Each key must be unique with each level
- Unique IDs
  - Generated by the CDK
  - Unique for every resource in an app
- Logical IDs
  - Based on the construct ID
  - Used by Cloudformation

### Contexts

Context values are key-value pairs that can be associated with a stack or construct. Essentally config values that can be applied to constructs.

These values can be sources from multiple locations, and may change based on the environment. The source could be a lookup on a resource such as a hosted zone, an availability zone, VPC, AMI lookup among others. The main source for user config is the `cdk.context.json` file.

### Assets

Local files, directories or Docker images that can be bundled into CDK apps. For example, a code file for a Lambda function or a static site bundle for an S3 bucket. Custom docker images can be uploaded to ECR and used in Fargate deployments.

Assets are cached in the `cdk.out` directory.

### Tagging

See: https://docs.aws.amazon.com/cdk/v2/guide/tagging.html

Tags are applied recursivly. A priority can be added to tags using the following syntax:

```python
Tags.of(my_construct).add("key", "value", priority=300)
```
