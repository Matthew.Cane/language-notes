# Ruby Notes

## General Comments

- Dynamic typing
- Heavily object oriented
- Very python like
- Close to natural language
- very little use of punctuation like brackets or semicolons
- Package manager is Gems, uses Gemfile

## Syntax

### Comments

- `#` for single line
- `=begin` and `=end` for multiline

### Printing

- `print var` for no new line
- `puts var` for new line
- `#{var}` in string to add variable to output

### Variables

- `name = "matt"`
- `counter += 1`
- 4 types of variables in Ruby:
  1. Local
  2. Global
  3. Instance
  4. Class
- Instance variables are instantiated with `@var`
- Class variables are instantiated with `@@var`

### User Input

- `gets` to take user input
- `gets.chomp` to remove newline character

### Strings

- `.upcase` for uppercase
- `.downcase` for lowercase
- `.capitalize` for pascal case
- `.include? "substring"` to check the contents of a string
- `.gsub(/s/, "substring")` replaces the regex with a substring. Called global substitution

### Conditionals

- If statements follow the syntax `if elsif else end`
- The `unless` keyword is equivalent to `if !var`. Can also be used as a way to block a line from running, e.g `problem = false; print "message" unless problem`

### Comparisons

- All pretty standard and self-explanatory

### Loops

- While loop looks like `while true end`
- Until loop looks like `i = 0 until i == 10 end`
- For loop (exclusive bounds) looks like `for i in 1...10  end`
- For loop (inclusive bounds) looks like `for i in 1..10  end`
- Iterator can be called with `loop {print "go"}` or with:

```Ruby
i = 0
loop do
  i += 1
  print i
  break if i > 5
end
```

- Objects can be iterated over using the `object.each do |i|` syntax
- Parts of a loop can be skipped over using the `next if` keyword
- For loop alternative syntax `10.times {}` or `10.times do |x| end`
- `1.upto(100) do |x|` will loop x 1 to 100

### Arrays

- Arrays created with `[1,2,3]` syntax
- Indexes can be accessed with `array[1]`
- Indexes can be accessed from the back with `array[-1]` for the last element

### Maps

- Maps created with `{"key" => "value"}` syntax
- Values can be found with `map["key"]` syntax

### Functions/Methods

- Create method with `fun doThing(foo, bar) end` syntax
- Arguments can be made optional with the `fun doThing(foo, bar=1)` syntax
- Functions are returned with the `return` keyword, or the last returnable statement if no `return` is present
- Multiple variables can be returned using the syntax `return foo, bar`. These are returned as an array

### Classes

Creating a class example:

```Ruby
class Person
  def initialize(name, age)
    @name = name
    @age = age
  end
  def printStudent
    puts "name: #{@name}"
    puts "age: #{@age}"
  end
end
```

- Class methods are created with `def self.methodName`. Class methods can only be called when using the class anonymously without creating an instance.
- Instance methods are created the normal way with `def methodName`.
- Inherit from a class with `class Student < Person`.

### Other

- use `!` at the end of a statement to assign to the value, e.g `name.capitalize!` sets name to `name.capitalize`
- Methods that end in `?` like `.include?` return a boolean

## Code Examples

Get user input, set to uppercase and return it to them:

```Ruby
print "User Input: "
user_input = gets.chomp.upcase
puts user_input
```

Iterate over an array:

```Ruby
array = [1,2,3,4,5]

array.each do |x|
  puts x
end
```

Execute a loop 10 times:

```Ruby
10.times do |x|
   puts x
end
```
