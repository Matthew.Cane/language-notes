# Salesforce

[Salesforce Documentation](https://trailhead.salesforce.com/en/content/learn/modules/platform_dev_basics)

## What is Salesforce?

It is a CRM (customer relationship manager) platform that incorporates bookkeeping, sales, marketing, e-commerce, customer service, and more. They call this collection of services 'Customer 360'.

## Apex

[Apex Developer Guide](https://developer.salesforce.com/docs/atlas.en-us.232.0.apexcode.meta/apexcode/apex_dev_guide.htm)

A proprietary programming language based on Java that is used to interact with the Lightning platform.

## Lightning

[Into to Lightning Platform](https://youtu.be/r9EX3lGde5k)

Salesforce's app development platform.

## SOQL & SOSL

[Introduction to SOQL and SOSL](https://developer.salesforce.com/docs/atlas.en-us.soql_sosl.meta/soql_sosl/sforce_api_calls_soql_sosl_intro.htm)

SOQL (salesforce object query language) is a proprietary query language based on SQL used to query salesforce objects.

SOSL (salesforce object search language) is a programmatic way of performing a text-based search against the search index.
