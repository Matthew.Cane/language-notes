# Kubernetes

Notes compiled by [Matthew Cane](https://gitlab.com/Matthew.Cane) from the following content:

- [Kubernetes Tutorial for Beginners by Nana.](https://youtu.be/X48VuDVv0do)
- [Kubernetes Documentation](https://kubernetes.io/docs/home/)
- [Just me and Opensource Tutorial Playlist](https://www.youtube.com/playlist?list=PL34sAs7_26wNBRWM6BDhnonoA5FMERax0)
- [Victor Farcis Tutorial Playlist](https://www.youtube.com/playlist?list=PLyicRj904Z9-L3XdyttvdPwRngIfGa52Y)
- [Kubernetes Glossary](https://kubernetes.io/docs/reference/glossary/?fundamental=true)

## What is Kubernetes?

- Official definition: 'Open source container orchestration tool'
- Developed by Google
- Helps manage containerized applications in different deployment environments, for example:
  - Physical
  - Virtual
  - Cloud
  - Hybrid

### What does Kubernetes solve?

- Software is transitioning away from 'Monoliths' to 'microservices' made up for hundreds or even thousands of containers
- Managing microservices is very complex, which is where container orchestration tools come in
  
### What features do orchestration technologies offer?

1. High availability
2. Scalability and high performance
3. Disaster recovery (backup and restore)

### The Layers of Abstraction

**Deployment** manages a...
**ReplicaSet** manage a..
**Pod** is an abstraction of a...
**Container**

Everything below a deployment should be managed automatically by K8s.

## Kubernetes Components

Kubernetes has lots of components, but typically you would use just a few:

### Pod

[Documentation page](https://kubernetes.io/docs/concepts/workloads/pods/)

- The smallest unit in K8s
- An abstraction layer over a container
- Abstracts away the [container runtime](https://kubernetes.io/docs/setup/production-environment/container-runtimes/), e.g. Docker, containerd, CRI-O
- You usually run run 1 container per pod, however you can run more than one such as a helper container
- Each pods gets it's own IP address internal to the K8s network
- Pods are ephemeral and can be recreated
- Recreated pods get a new IP address each time
- Pods are not directly created, they are created by the nodes Kubelet via a deployment
- Pods are created as part of a [deployment.](#deployment)

#### Multi-container Pods

You can have as many containers in a pod as you like, however k8s can not scale containers within a pod so for scaling this is not recommenced.

Containers within a pod share the same IP address so you can refer to other containers using `localhost:port`.

### Node

[Documentation page](https://kubernetes.io/docs/concepts/architecture/nodes/)

- A physical or virtual server that the cluster runs on
- Will have multiple pods running on them
- Can be either a master or worker node

Three processes must run on every node, master or otherwise:

1. Container runtime: Runs the actual containers on the hardware
2. Kubelet: A K8s process. Takes the configuration, starts the pod with a container inside, and assigns resources to the container
3. KubeProxy: Facilitates the communication between pods and [services](#service). Will intelligently forward messages to the closest pod, usually on the same node

### Service

[Documentation page](https://kubernetes.io/docs/concepts/services-networking/service/)

- A static, permanent IP address that is pointed to one or many [pods](#pods)
- The lifecycle of a service and a pod are not connected
- Also acts as a load balancer between multiple pods in the same service
- Services always exist alongside deployments

There are four types of services:

1. ClusterIP: Internal and default. Only accessible from inside the K8s network
2. NodePort: Maps the IP of each node that the deployment is running in to a single static port. If an external request comes in to any of the nodes on the NodePort port, it will route the request to the relevant service to be load balanced
3. LoadBalancer: Assigns the service an external IP address through a network load balancer (NLB). This is usually provided by the cloud infrastructure provider
4. ExternalName Maps the service to the `externalName`, e.g. a URL

### Ingress

[Documentation page](https://kubernetes.io/docs/concepts/services-networking/ingress/)

[Ingress in 5 mins video](https://youtu.be/NPFbYpb0I7w)

Forwards requests from the ingress IP address to the appropriate service based on the routing rules defined within the ingress component configuration.

Below is an example configuration file for an ingress component:

```yaml
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: app-ingress
spec:
  rules:
  - host: example.com
    http:
      paths:
      - backend:
        serviceName: app-service
        servicePort: 8080
```

The `spec` section defines routing rules:

- `host` defines the domain or IP address of the node that will receive the requests
- `paths` define the paths for the URL, for example `/api/whatever`
- `backend` is where the incoming requests will be redirected to

### Ingress Controller

[Documentation page](https://kubernetes.io/docs/concepts/services-networking/ingress-controllers/)

- This is a pod that receives the incoming request and applies the ingress routing rules to the packets
- In a cloud environment, the provider will create a virtual load balancer as an entrypoint but baremetal configurations will need this set up separately
- There are many types of ingress controllers. The default is a K8s Nginx based controller, but there are many 3rd party solutions

It is typical to use helm to install an ingress controller (load balancer):

`helm upgrade ingress-nginx ingress-nginx/ingress-nginx --namespace kube-system`

Using the `--namespace kube-system` keeps the ingress controller in the system namespace, away from application nodes.

`kubectl get ingress` shows the external IP that the ingress service has been given by the ingress controller.

### Config Map

[Documentation page](https://kubernetes.io/docs/concepts/configuration/configmap/)

- Contains external configuration for your application, such as URLs
- Connected to a pod so the pod has access to the configs
- In plain text, so don't put passwords, usernames, keys, e.t.c. in here, use [secret](#secret)

An example config map can be seen below:

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: mongodb-configmap
data:
  database_url: mongodb-service
```

This sets the database_url pointing to the service that houses mongodb pods.

### Secrets

[Documentation page](https://kubernetes.io/docs/concepts/configuration/secret/)

- Similar to config map, but secrets must be stored in a base64 format
- Connect it to a pod

An example secret can be seen below:

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: mongodb-secret
type: Opaque
data:
  username: dXNlcm5hbWU=
  password: cGFzc3dvcmQ=
```

### Volumes

[Documentation page](https://kubernetes.io/docs/concepts/storage/volumes/)

- Volumes are ephemeral and are tied to the lifetime of the pod
- Mounts one or more directories to a pod
- Volumes can be shared between containers in a pod

The default volume type is `emptyDir` and creates an empty directory

### Persistent Volumes

[Documentation page](https://kubernetes.io/docs/concepts/storage/persistent-volumes/)

- Persistent volumes persist even after the pod is deleted
- Independent of the lifetime of pods and separately managed
- One or more pods can claim a persistent volume if the storage supports multiple readers/writers
- Can be provisioned in advance or dynamically
- Pods provision storage using a Persistent Volume Claim (PVC)

The persistent disc can be many different storage types such as:

- GCE Persistent Disks
- Azure Disks
- Amazon EBS (Elastic Block Storage)
- iSCSI
- NFS

A PVC will define the access mode and storage requirements, and k8s will automatically bind it with the most appropriate persistent volume backend

The PVC then must be mounted to the containers within the pod using a `volumeMount`

![Persistent Volumes](k8s-persistent-volumes.png)

There are three kubernetes volume access modes:

- ReadOnly
- ReadWriteOnly
- ReadWriteMany

### Deployment

[Documentation page](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/)

- A blueprint of how pods are deployed. This could be the type of pod or the number of pods
- This allows for replicas of pods to be deployed automatically to different nodes. A deployment is declarative
- Stateful pods (Databases) can NOT be replicated to avoid data inconstancy. To solve this, use StatefulSet

### StatefulSet

[Documentation page](https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/)

- Used to deploy stateful pods, i.e. databases
- Stateful sets are not easy
- It's common to host stateful services (databases) outside the cluster and only host stateless services within K8s

### DaemonSet

[Documentation page](https://kubernetes.io/docs/concepts/workloads/controllers/daemonset/)

- Defines pods that are to be deployed to every node, or every node matching certain parameters
- Can be used for:
  - Used for monitoring of nodes, logs collection
  - Opening a specific port on every node like 22 for SSH
  - Cluster management services like KubeProxy
  - Networking management services such as getting the VPC IP in AWS

## Kubernetes Architecture

What are master and worker nodes, what they do, and how a cluster is self-managed and self-healing.

### Master Nodes

Master nodes control everything within a cluster. The master nodes are collectively known as the control plane.

There are 4 "master components" that run on every master node (usually in addition to the services that usually run on a node):

1. API server: How admins interact with the cluster. Validates requests and acts as an authentication gatekeeper
2. Scheduler: After receiving a command from the API server, it decides intelligently where to start new pods based on the nodes resources. Passes the request to start to the Kubelet on the node
3. Controller manager: Detects state changes such as the crashing of pods and tries to recover the pods by making a request to the scheduler
4. etcd: A key-value store of the cluster state. Every change in the cluster is stored in etcd. Scheduler and controller refer to etcd to query the state of the cluster. Essentially metadata about the cluster

- There are often multiple masters, and in that case etcd replicates the cluster state across the master nodes
- Must have an odd number of masters?
- Master nodes do not need as many resources as worker nodes
- Master nodes are part of the control plane

### Replica Set

- A replica set manages the replicas of a pod
- All deployments are part of a replica set wether of not they are actually replicated
- The [service](#service) will load ballance across the replica set
- Replica sets are automatically created and managed

## Naming Conventions

When created, a pod will be named after the deployment name, then the replica set ID, then it's own ID. For example, `nginx-depl-5c8bf76b5b-2dt7w`.

## Namespaces

Resources can be organised into namespaces, kind of like a virtual cluster inside a cluster

- There are a number of default K8s namespaces
  - default: The default one
  - kube-node-lease: Contains information on each node, it's availability
  - kube-public: Publicly accessible data. Contains a configmap that contain cluster info without needing authentication
  - kube-system: Not meant for user use. Contains system processes like CoreDNS

To create a namespace use `kubectl create namespace {namespace-name}` or by using a namespace manifest file.

It is best to separate complex applications, projects, and teams into separate namespaces to prevent configuration file clashes. Namespaces can have their resources limited.

Components are able to communicate between namespaces, however there are some limitations; Components are unable to access the config maps and secrets of other namespaces. However components are able to access the services within other namesspaces using the `service-name.namespace` syntax.

Some components do not live within a namespace and are globally accessable, such as:

- [Volumes](#volumes)
- [Nodes](#node)

The default namespace is used for all commands unless the `-n {namespace-name}` is used. A tool called [kubens.](https://github.com/ahmetb/kubectx/blob/master/kubens)

## Labels

[Documentation page](https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/)

- Key/value pairs attached to an object
- Are used to identify objects that are meaningful to the users
- Are often used to assist UI and CLI tools in understanding the stack
- They are found within the metadata section of a configuration file

Some examples are:

```yaml
labels:
  app: nginx
  release: stable
  environment: dev
  tier: backend
  partition: customerB

```

- Labels can contain dashes, underscores and dots and must be less than 63 characters
- They can be referred to using label selectors

## Selectors

- A selector refers to a label
- These are found within the specification section of a configuration file
- They link services and pods to deployments

For example, a deployment may have the label `app: nginx`. The template for the pods within that deployment have a selector to match the `app: nginx` label of it's deployment. A service will also have a selector of `app: nginx` to link it to the pods and the deployment.

## Interacting With Kubernetes

There are a few ways of interacting with a cluster:

1. Directly with the K8s API server on the control plane. This is not done very often, typically you would use a...
2. Client Library. These can be used within programming languages. K8s maintains official client libraries for Go, Python, Java, .NET and JS. There are many community libraries.
3. Kubectl. See [kubectl.md.](https://gitlab.com/Matthew.Cane/language-notes/-/blob/master/Kubernetes/kubectl.md)
4. Web dashboard. This is an optional addition.

## Service Discovery Methods

There are two ways for pods to discover services within the cluster and namespace:

- Environment Variables
  - K8s will automatically inject environment variables into containers that provide the address to access the services
  - Environment variables follow a naming convention based on the service name
  - All a pod needs to know to connect to a service this way is the name of the service
- DNS
  - K8s automatically constructs A records within the clusters internal DNS
  - Containers are automatically configured to query these DNS records

### Environment Variable Service Discovery

If, for example, an app needs to connect to a Redis instance in a pod connected to a service called `data-tier` it could inject the environment variables `$(DATA_TIER_SERVICE_HOST)` and `$(DATA_TIER_SERVICE_PORT_REDIS)` into the pod in the manifest file to find the hostname and port of the Redis instance. This is assuming that when the `data-tier` service was defined the exposed port was named `redis`. These environment variables are created by k8s. Note that k8s does not update the environment variables of running containers so the service must be created before the pod. They must also be in the same namespace.

### DNS Service Discovery

The IP address of a service can be referenced within the manifest file using DNS with the syntax `{service-name}.{service-namespace}`. This allows the pod to connect to services in other namespaces If the service is in the same namespace you can just use the service name. To connect to a specific port using DNS discovery you can either hard-code the port name or use the environment variable method as described above to get the exposed port.

### Autoscaling

Autoscaling allows for k8s to scale a deployment automatically based on:

- CPU utilization or other custom metrics
- Minimum and maximum replica targets
- CPU utilization is based on a value defined in the manifest. It will scale the number of pods if the CPU utilization goes above or below this value

Autoscaling requires that a metrics server is running on the cluster. This is maintained by kubernetes [in this git repo](https://github.com/kubernetes-incubator/metrics-server) and can be installed by running this command:

`kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml`

Once the metrics server is running the following commands become available:

- **`kubectl top pods`:** This shows the CPU and memory usage of pods

### Update Rollouts

[Kubernetes deployment strategies](https://blog.container-solutions.com/kubernetes-deployment-strategies)

A change to an existing deployments pod template triggers a rollout. Deployments can have different rollout strategies:

- Ramped (default). Replicas are updated in groups rather than all at once in a rolling fashion. This minimizes downtime, however during the rollout old and new versions of the deployment will be running at once.
- Recreate. Old version is terminated then new one is created. This incurs downtime

Scaling events do not trigger an rollout. Rollouts can be undone using the 'rollout undo' command. See the kubectl notes for more information.

### Probes & Health checks

Probes are used to check the internal status of a pod. There are two types of probes:

- Readiness probe: Used to check when a pod is ready to serve traffic. Services use the ready condition to tell if the pod should be sent traffic
- Liveness probe: Used to detect if the pod has entered a broken state. When this happens k8s will restart the pod for you

information on probes:

- Probes can be declared on a pods containers.
- All containers within the pod must pass for the pod to pass
- A probe action could be running a command within the container, doing an HTTP GET request or opening a TCP socket
- By default probes check containers every 10 seconds
- By default a probe must fail 3 times to be marked as failed

### Init Containers

- Init contains are a way to run initialization tasks that are required to be run before the main container starts
- They are separate from the main container image but are tightly coupled and run in the same pod
- Pods can declare any number of init containers, and are run in the order in which they are declared
- They can be used to delay or block the starting of a container until defined preconditions are met
- Init containers are run every time a pod is started or restarted, including every replica
- Init containers do not contain any readiness probes
