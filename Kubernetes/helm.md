# Helm

- [Documentation page](https://kubernetes.io/blog/2016/10/helm-charts-making-it-simple-to-package-and-deploy-apps-on-kubernetes/)
- [Helm docs](https://helm.sh/docs/)

## What is Helm?

- A package manager and template engine for K8s
- Used to package yaml files and distribute them to public and private repositories
- A bundle of yaml is called a helm chart
- Commonly used applications can ebe found pre-made, such as databases, monitoring apps, [elasticsearch.]([?](https://www.knowi.com/blog/what-is-elastic-search/))
- Charts can be downloaded from public or private repositories

## v2 vs v3

- v2 uses a server called Tiller that sits in the K8s cluster. A CLI tool calls Tiller to run the commands
- This allows for release management. Tiller tracks release history, so in the event of a deployment failure it can roll back automatically
- Tiller requires a lot of permissions and can be a security risk
- Helm v3 does not need Tiller

## Templates

Helm also acts as a template engine, which is useful for deploying microservices. Values that can be changed are defined in a template file:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: {{ .Values.name }}
spec:
  containers:
    - name: {{ .Values.container.name }}
      image: {{ .Values.container.image }}
      ports: {{ .Values.container.port }}
```

- The values come from an external file called `values.yaml` which contains defaults
- Default values can be overwritten when executing the chart

## Helm Chart Directory Structure

The top level folder is named after the chart
### `chart.yaml`

Contains metadata about the chart. An example is:

```yaml
apiVersion: v2
name: application-name
description: A description of the application
type: application
version: 1.0.0
appVersion: 1.0.0
```

- The `type` field can be either 'application' or 'library'
- The `version` is the version of the helm chart and `appVersion` is the version of the application being deployed
- There are many other optional field, see documentation

### `values.yaml`

Contains a structured list of default values which at template rendering time get injected into the chart templates that reference values file. For example:

```yaml
service:
  type: ClusterIP
  port: 80
```

To reference the values within the values file, use a dot notation. For example, if you wanted to get the port number from the previous example values file you would use `.Values.service.port`

These values can be overridden during installation by using the `--set` parameter.

### `charts` Directory

Contains other charts that are dependencies of the main chart

### `templates` Directory

Contains the definitions for creating resources in the cluster in the for of manifest files. All of the files within the template directory are subject to the template rendering process.

A `NOTES.TXT` file can be added to the templates directory that contains instructions to the end-user of the chart as to how the deployed application should be used. This is printed to the terminal after running `helm install` or `helm upgrade` on the chart. The NOTES file also undergoes the template rendering process which allows for customization of the notes printed to the console.

The `_helpers.tpl` file...

### `tests` Directory

This can contain one or more tests that can be written to validate the chart once deployed. Tests are usually implemented using a k8s job or pod resource and should be designed to return a 0 exit code so helm knows that the test has succeeded. Non-zero exit codes represent test failures.

## Template Files

[Template file documentation](https://helm.sh/docs/chart_template_guide/getting_started/)

The template files are kubernetes manifest files with additional templating syntax added. The template rendering process looks for values within double curly brackets, evaluates them and generates the final manifest files that are passed to the cluster api server. Templates are golang based.

Below is an example of a template for a service resource:

```yaml
apiVersion: v1
kind: Service
metadata:
 name: {{ include "webserver.fullname" . }}
 labels:
   {{- include "webserver.labels" . | nindent 4 }}
spec:
 type: {{ .Values.service.type }}
 ports:
   - port: {{ .Values.service.port }}
     targetPort: http
     protocol: TCP
     name: http
 selector:
   {{- include "webserver.selectorLabels" . | nindent 4 }}
```

## Important Notes

- Template directions are enclosed within double curly braces with white-spaces
- There are in-build values. Consult the documentation
- `indent` and `nindent` add indentation to the rendered template, the difference being that `nindent` adds a new line to the start of the string
- Adding a - after or before the double curly braces will trim the whitespace on that line, for example `{{- .Values.name }}`

## Helm Commands

### Helm Search Hub

Used for fuzzy searching of searching publicly registered charts in the Helm Hub

### helm search repo

Used for searching for charts in your local repository

### helm repo add

The helm repo add command takes both a name and URL as its arguments. The name is used to reference the repository later on, when using other helm commands such as helm install or helm upgrade. The URL refers to the location of the repository, which can be either public, hosted on the Internet, or private, hosted on an internal network.

### helm repo list

The helm repo list command simply lists out the registered helm repository list, including both name and URL for each registered repository. This command can take a --output parameter which alters the format of the outputted list. This parameter can be set to any of the following values, table, JSON, or yaml, with table being the default when this parameter is absent.

### helm repo remove

The helm repo remove command takes a repository name and then removes that particular repository from the currently configured repository list.

### helm repo update

The helm repo update command updates the latest information about charts from the respective chart repositories. Chart information is cached locally, where it is then queried on by other helm commands, for example the helm search repo command.

### helm pull

`helm pull <repo name>/<chart>`

The helm pull command is used to pull down a package from a package repository. When running this command, you provide it either with a chart URL or string composed of the repository name, chartname. As the documentation states, this is useful for fetching packages to inspect, modify, or repackage. It can also be used to perform cryptographic verification of a chart without actually installing the chart.

### helm install

The helm install command is used to perform an installation of a chart. When installing a chart, you specify a name for it, together with the location of the chart itself. The location of the chart can be specified one of five different ways. A typical approach will be to specify the registered repository name, slash name of the chart. For example to install the wordpress chart with the name wordpress located in the locally-registered bitnami repository, use the command:

`helm install wordpress bitnami/wordpress.`

To see the rendered version of a particular file without applying it to the cluster you can use the command:

`helm install --debug --dry-run <release name> <chart path>`

### helm upgrade

The helm upgrade command is used to upgrade an existing chart release, and results in a new revision being created for the release once applied. When running this command, you must specify both the release name and the chart. The chart itself may contain updates that potentially changes the behavior of the deployed application. Or equally, the chart may be unchanged and instead new values are being passed in as part of the helm upgrade command, which again results in a new revision and change of behavior.

### helm rollback

The helm rollback command is used to rollback a chart release to a specific revision number. The first argument of the rollback command is the name of a release, and the second is a revision, or version number. If the second argument is omitted, it will roll back to the previous release. Note, this command can be used to move the current release back and forward. The revision history for a release can be displayed by running the command helm history together with the release name.

### helm uninstall

The helm uninstall command takes a release name and uninstalls the release. It removes all of the resources associated with the last release of the chart, as well as the release history, freeing it up for future use. Moving on, the following group of commands are focused on managing and maintaining Helm repositories.
