# Manifest Files

![config.yaml example](k8s_config_diagram.png)

Manifest files are in the `.yaml` format. Each manifest file has three parts:

1. Metadata
2. Specification
3. Status (This is automatically added and edited by K8s)

If there is a difference between the specification and the status, K8s knows it needs to take action to rectify the disparity.

- A manifest file requires at a minimum:
  - `apiVersion`
  - `kind`
  - `metadata`
  - `spec`
- The `template` section within the `spec` section describes the individual pods

Below is an example manifest file for a very basic Nginx deployment:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx
spec:
  replicas: 1
  selector:
    matchingLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image:  nginx
        ports:
        - containerPort: 80
```

Kubernetes can detect if you are trying to create a new deployment or edit an existing one. When editing a deployment, Kubernetes will start the new deployment and then destroy the old one.

Below is an example manifest file of an internal service component for the above nginx deployment:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx-service
spec:
  selector:
    app: nginx
  ports:
    - protocol: TCP
      port: 80
      targetPort: 8080
```

To add the component to a namespace, add `namespace: {namespace-name}` within the metadata section.

## Environment Variables

[Documentation page](https://kubernetes.io/docs/tasks/inject-data-application/define-environment-variable-container/)

TBD: See mongo-deployment.yaml example

## Resource Requests

Resource requests are a way to specify the hardware requirements of a pod deployment. Without a resource request, k8s will assign pods in a `BestEffort` mode, which moves the pods between clusters in a way that provides the best overall balancing. With a resource request, this guarantees that the pod will have the resources requests. If the resources are not available, it will not be scheduled by the scheduler.

In production is it recommended to define a resource request to ensure all the pods run as expected.

Below is a manifest file containing a resource request.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: mypod
  labels:
    app: webserver
spec:
  containers:
  - name: mycontainer
    image: nginx:latest
    resources:
      requests:
        memory: "128Mi" # 128Mi = 128 mebibytes
        cpu: "500m"     # 500m = 500 milliCPUs (1/2 CPU)
      limits:
        memory: "128Mi"
        cpu: "500m"
    ports:
      - containerPort: 80
```

## imagePullPolicy

[Kubernetes Docs](https://kubernetes.io/docs/concepts/containers/images/#updating-images)

In the container section, by default imagePullPolicy will be set to `ifNotPresent` which will only pull a new image if the image is not present within k8s.

If the image tag is set to `:latest` the policy will be set to `always`. This will make k8s always pull an image down when starting the container.

The pull policy can be manually set using the following:

```yaml
spec:
  containers:
    - name: redis
      image: redis:latest
      imagePullPolicy: IfNotPresent
      ports:
        - containerPort: 6379
```

## Horizontal Pod Autoscaling

[Horizontal Pod Autoscaler Walkthrough](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale-walkthrough/)

An autoscaler manifest defines a horizontal pod autoscaler (HPA) resource type. An example of which is:

```yaml
apiVersion: autoscaling/v1
kind: HorizontalPodAutoscaler
metadata:
  name: app-tier
  labels:
    app: microservices
    tier: app
spec:
  maxReplicas: 5
  minReplicas: 1
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: app-tier
  targetCPUUtilizationPercentage: 70
```

This maifest defines an autoscale policy targeting the `app-tier` deployment. It states a minimum of 1 replicas and a max of 5 and will scale the replica count if the CPU utilization is outside +-10% of the target utilization. This is equivalent to the standalone command `kubectl autoscale deployment app-tier --max=5 --min=1 --cpu-percent=70`

## Probes

A probe is described as part of a deployment within the pod spec. Below is an example of liveness and readiness probes for a Redis pod:

```yaml
livenessProbe:
  tcpSocket:
    port: redis # named port
  initialDelaySeconds: 15
readinessProbe:
  exec:
    command:
    - redis-cli
    - ping
  initialDelaySeconds: 5
```

## Init Containers

Described as part of a deployment.

```yaml
initContainers:
  - name: await-redis
    image: lrakai/microservices:server-v1
    env:
    - name: REDIS_URL
      value: redis://$(DATA_TIER_SERVICE_HOST):$(DATA_TIER_SERVICE_PORT_REDIS)
    command:
      - npm
      - run-script
      - await-redis
```

## Persistent Volumes

A persistent volume manifest defines a static persistent storage volume. Below is an example persistent volume in AWS EBS:

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: data-tier-volume
spec:
  capacity:
    storage: 1Gi # 1 gibibyte
  accessModes:
    - ReadWriteOnce
  awsElasticBlockStore: 
    volumeID: INSERT_VOLUME_ID # replace with actual ID
```

## Persistent Volume Claim

A persistent volume claim is defined below:

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: data-tier-volume-claim
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 128Mi # 128 mebibytes
```

The claim is then referenced within the container spec so the containers within this pod will have access to the persistent volume.

```yaml
      volumes:
      - name: data-tier-volume
        persistentVolumeClaim:
          claimName: data-tier-volume-claim
```

## Volume Mounts

These are described within a container definition. Theses can reference persistent volume claims or ephemeral local volume mounts. Below is an example of a PVC mount:

```yaml
volumeMounts:
  - mountPath: /data
    name: data-tier-volume
```
