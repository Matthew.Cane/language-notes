# Kubectl

[Kubectl documentation reference](https://kubernetes.io/docs/reference/kubectl/)

[Kubectl cheat sheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/)

## What is Kubectl?

- A command-line tool for Kubernetes
- Submits commands to the K8s master node API server

## Starting a local Minikube cluster

[Minikube documentation](https://minikube.sigs.k8s.io/docs/)

- Simply run `minikube start` in the terminal to start up
- Run `minikube stop` to terminate
- Use `minikube status` to show the status of the cluster

## Kubectl Commands

**See version:** `kubectl version`

### `get` Commands

The `get` command followed by any K8s component shows information on the given components, for example:

- **Get all nodes:** `kubectl get nodes`
- **Get all pods:** `kubectl get pod`
- **Get all services:** `kubectl get services`
- **Get all deployments:** `kubectl get deployment`
- **Get all replica sets:** `kubectl get replicaset`
- **Get all ingress instances:** `kubectl get ingress`
- **Get everything:** `kubectl get all`

- Adding the name of a component will display information on that specific component.
- Adding `-o wide` to the end will display verbose information.
- Adding `-o yaml` to the end will display the information in a yaml format. Pipe this to a file for easier reading.
- Adding `-w` will watch the file and print any changes to the terminal
- This can also be done using the linux command `watch -n 1 kubectl watch {whatever}` which is nicer

### `create` Commands

The `create` command is used to create new K8s components. The most basic example takes the format: `kubectl create {component-type} {component-name}`

Example `create` command that creates an nginx deployment:

`kubectl create deployment nginx-depl --image=nginx`

### `edit` Commands

The `edit` commands can be used to modify existing components using the format `kubectl edit {component-type} {component-name}`. This displays the .yaml file that describes the component for editing.

### `logs` Command

The `logs` command returns the logs for a given component, for example `kubectl logs {component-type}`

### `describe` Command

Prints detailed information about a resource of a group of resources.

`kubectl describe pod [pod-name]`

- Under the description of a pod there will be an `Events` section. This is very useful for debugging.
- When describing a service, the endpoint will be the IP of the pod that it is associated with. There should be one IP for each pod

### `exec` Command

Acts like the exec command in Docker, allows you to enter the container within a given pod, for example:

`kubectl exec -it {pod-name} -- bin/bash`

### `delete` Command

Remove a deployment with `kubectl delete deployment {deployment-name}`. This can also be used with configuration files with `kubectl delete -f {file-name.yaml}`

### `apply` Command

The `apply` command takes a K8s configuration file, prefaced with `-f`, and executes it. For example:

`kubectl apply -f {file-name.yaml}`

To apply the file in a different namespace, append the command with `--namespace={namespace-name}`, for example

`kubectl apply -f {nginx-depl.yaml} --namespace=ingress`

### `logs` Commands

The `logs` command will get the logs of the service you point it to, for example:

`kubectl logs {app-name}`

There are a number of useful extra options such as:

- `-f` to follow the logs
- `--tail {int}` to see the last {int} logs

### `scale` Command

Used for modifying replica counts in running deployments. Fore example:

`kubectl scale deployments {deployment-name} --replicas=5`

### `api-resources` Command

`kubectl api-resources` lists all possible resources in k8s and their shorthands. Use this as a reference to prevent you from typing out long resource names.

### `edit` Command

This can be used to edit existing manifests in the cluster. It opens the resource manifest in vi. The default editor can be changed by creating an environment variable called `KUBE_EDITOR` containing the path to the editor you want to use. However, it is recommended to change the .yaml file and re-apply it.

### `Rollout` Command

The rollout commands are used to monitor and control the rollout of new deployments

- **`kubectl rollout status deployment {deployment-name}`**: Shows the progress of deployment rollouts
- **`kubectl rollout pause deployment {deployment-name}`**: Pauses the progress of deployment rollouts
- **`kubectl rollout resume deployment {deployment-name}`**: Resumes the progress of deployment rollouts
- **`kubectl rollout undo deployment {deployment-name}`**: Rolls back the deployment to the previous revision
- **`kubectl rollout history deployment {deployment-name}`**: Shows the revision history of a deployment
- **`kubectl rollout undo deployment {deployment-name} --to-revision={revision-number}`**: Rolls back the deployment to the specified revision
- **`kubectl rollout restart deployment {deployment-name}`**: This restarts all the pods in the deployment. This can be used to allow new config map values to be loaded into the applications

### `port-forward` Command

Allows a port on a specific pod to be forwarded to the localhost of your machine. Extremely useful for debugging an app. Example of connecting to a Prometheus server:

`kubectl -n prometheus port-forward prometheus-1625564388-server-7cbb446446-86n8q 9090`

More generic command template:

`kubectl port-forward {pod-name} {port-number}`