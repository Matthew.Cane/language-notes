# Terraform

Notes compiled by [Matthew Cane](https://gitlab.com/Matthew.Cane) from the following content:

- [Terraform Course - Automate your AWS cloud infrastructure](https://youtu.be/SLB_c_ayRMo)
- [Terraform Docs](https://www.terraform.io/docs/index.html)

Examples and code snippets will use AWS as the provider.

## What is Terraform?

Terraform is a declarative, infrastructure as code configuration tool that integrates with almost every cloud provider.

## Terraform CLI

The CLI tool enables the execution and management of terraform configuration files.

### Init

`terraform init` will look at the `.tf` files in the current directory, download the plugins required to run the files and prepare the backend to execute the files.

### Plan

- `terraform plan` will see all the changes that terraform is required to make to bring the current state of your system inline with the state of your system as described by the `.tf` files
- This is a dry run and will not have any effect, and is recommended to check which changed are going to be made.
- Many of the values will be displayed as `(known after apply)`. These will fill themselves in once the plan has been executed

## Apply

This will execute the changes shows in the plan.

## Terraform Configuration File

- Terraform config files are written in hashicorp configuration language with a `.tf` extension

### Provider

First thing to define is a [provider](https://registry.terraform.io/browse/providers) i.e. AWS, Linode, Azure, K8s, VMware... An example provider config would be:

```conf
provider "aws" {
  region = "eu-west-2"
}
```

### Authentication

- There are two mains ways to authenticate, static and dynamic
- Static authentication is **NOT** recommended as credentials can be accidentally leaked to source-control.

Static authentication can be done by passing the access and secret keys into the provider clause:

```conf
provider "aws" {
  region = "eu-west-2"
  access_key = "my-access-key"
  secret_key = "my-secret-key"
}
```

The more secure way to apply secrets and other variables is with environment variables in Gitlab CI, for example:

```conf
provider "aws" {
  region = "$AWS_DEFAULT_REGION"
  access_key = "$AWS_ACCESS_KEY_ID"
  secret_key = "$AWS_SECRET_ACCESS_KEY"
}
```

### Resource

Terraform will translate a resource clause into the correct API calls for your provider. An example resource of deploying an EC2 instance is as follows:

```conf
resource "aws_instance" "instance-name" {
  ami = "ami-09a56048b08f94cdf"
  instance_type = "t2.micro"
}
```

- The first two sections after the resource keyword are the resource type and a name for your resource. The name can be whatever you choose
- The AMI, or Amazon Machine Image, is the base image that will be loaded onto your instance
- The instance type is the size of the instance

All of the key-value configuration will be specific to the resource type, so the documentation must be referred to.
