
# Sets provider to AWS, insert keys where needed
provider "aws" {
  region = "eu-west-2"
  access_key = "my-access-key"
  secret_key = "my-secret-key"
}

# Creates an EC2 instance using the image 
# Ubuntu Server 18.04 LTS of size t2.micro called "ubuntu-server" 
resource "aws_instance" "ubuntu-server" {
  ami = "ami-09a56048b08f94cdf"
  instance_type = "t2.micro"
}