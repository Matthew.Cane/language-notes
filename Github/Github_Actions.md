# Github Actions

Notes compiled by [Matthew Cane](https://gitlab.com/Matthew.Cane) from the following content:

- [GitHub Actions Tutorial - Basic Concepts and CI/CD Pipeline with Docker](https://www.youtube.com/watch?v=R8_veQiYBjI)
- [GitHub Actions Tutorial | From Zero to Hero in 90 minutes (Environments, Secrets, Runners, etc)](https://www.youtube.com/watch?v=TLB5MY9BBa4)
- [Github Actions Documentation](https://docs.github.com/en/actions/learn-github-actions)

## What is Github Actions?

A developer automation tool on the Github platform.

## Key Concepts

### Events

An event is any action that can trigger a pipeline. A complete list can be found [here](https://docs.github.com/en/actions/using-workflows/events-that-trigger-workflows).\

