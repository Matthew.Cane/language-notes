tree --noreport -I 'README.md|genDirTree.sh|.gitlab-ci.yml' -H https://gitlab.com/Matthew.Cane/language-notes/-/blob/master |
sed -n -e '/<body>/,$p' |
sed -e '0,/.*https.*/ s/.*https.*//' |  
pandoc --from html --to markdown_strict |
head -n -12 > README.md