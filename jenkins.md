# Jenkims

Notes compiled by [Matthew Cane](https://gitlab.com/Matthew.Cane) from the following content:

- [Complete Jenkins Pipeline Tutorial | Jenkinsfile explained](https://www.youtube.com/watch?v=7KCS70sCoK0)

## What is Jenkins?

A CI/CD system

## Jenkinsfiles

- Jenkinsfiles are the files used to define pipelines
- They are created as a file in the top level of your project directory called `Jenkinsfile`
- There are two types of Jenkinsfiles, **scripted** and **declarative**
- Scripted Jenkinsfiles use the Groovy scripting language
- Declarative Jenkinsfiles are more 

### Declaritave

The most basic boilerplate Jenkinsfile looks like this:

```groovy
pipeline {

    agent any

    stages {

        stage("build") {

            steps {}
        }
    }
}
```

The `steps{}` block contains groovy script that is executed in squence.

#### post

There is also an optional `post` block that can be placed under `pipeline` that can trigger jobs under certain conditions:

```groovy
post {
    always {}
    failure{}
    success{}
}
```

#### Stage Conditions

You can set conditionals for each stage, for example this stage will only execute if the current branch is `whatever`:

```groovy
stage("test") {
    when {
        expression{
            BRANCH_NAME == "whatever"
        }
    }
    steps{}
}
```

### Scripted

A Jenkinsfile using the *scripted* format is simpler:

```groovy
node {
    stage("build") {

    }
}
```

### Environment Variables

Environment variables are used a lot in Jenkins. To see a complete list of env vars that are availible in your server, go to `https://<server-url>/env-vars.html`. Plugins will affect what env vars are availible for you to use.

You can of course define your own env vars using a block in the pipeline block:

```groovy
environment {
    FOO = "bar"
}
```

You can also define secret variables in the Jenkins server and refer to them using the following syntax:

```groovy
environment {
    SECRET = credentials('name-of-secret')
}
```

However this requires a plugin which you have to install first
