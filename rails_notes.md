# Ruby On Rails Notes

## General Comments

- Model-View-Controler (MVC) based.
- Webpack can be used for front end frameworks.
- scaffold? scaffold css?
- Requires a lot of testing.
- Uses SQlite3 (yay!)
- Automatically generates SQL commands.
- Has an API mode with `--api` when creating new project.

## Setup

- [Link for instructions](https://gorails.com)
- Awkward to run on Windows

To create a new project:
`rails new project-name` creates a new project in the CWD.

## Basic Rails commands

To run a server:
`rails server`

Create a new model, or anything else:
`rails generate model model_name`

There are loads of different things you can generate, like:

- Models
- Views
- Controllers
- Tests
- Helpers
- Mailers

Scaffold is used to generate models with database tables
`rails generate scaffold model_name entry:datatype`

## Active Record

- The 'Model' part of Model-View-Controller
- Facilitates the creation and use of business objects
  - Business objects require persistent database storage
- Active records contain persistent data and the behaviors which operate on the data.
- **Object Relational Mapping**, or ORM, connect rich application objects into relational tables.
- ORM allows data to be retrieved from a relational database without using SQL
- Uses singular CamelCase (pascal case?) and singular nouns for class names, e.g `BookClub`
- Rails pluralizes the database table names, and translates them to snake case, e.g `book_clubs`
- Automatically adds `created_at`, `updated_at`, `lock_version`, `type`, `(association_name)_type`, and `(table_name)_count`
  
### Creating Active Record Models

```Ruby
class Product < ApplicationRecord
end
```

This will create a 'Product' active record

## Action View

- Concerned with communicating with the database and performing CRUD actions
- Action View templates are written using embedded Ruby in tags mingled with HTML
- Share a name with their associated controller action
- Files have the filetype of `.rhtml`

`Link_to` is used to refer to items within the controller.

## Action Controller

- The 'Controller' in MVC
- Responsible of making sense of the request and producing the appropriate output
- Middleman between models and views
  
Created with the syntax:

```Ruby
class ClientsController < ApplicationController
  def new
    render :text => "New Client"
  end
end
```

- When a client goes to /clients/new, the application creates an instance of `clientsController` and calls the function `new` which displays some text.
- The first method to create would usually be `index`.
- A controller method would usually point to an action view template.

## Routing

- Rails recognizes URLs and dispatches them to the appropriate controller.
  
If a `get` request is sent to `/patients/17` then the `patients` controller will call the `show` action with `{id: '17'}`

## Asset Pipeline
